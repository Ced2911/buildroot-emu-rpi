################################################################################
#
# LIBRETRO_ARMSNES
#
################################################################################
#LIBRETRO_ARMSNES_VERSION = ac83fb25f0c7f59350b92819e83e8fdecb7fc397 #b38bccea1210febbe0ee8df051481e0098ce7681
#LIBRETRO_ARMSNES_SITE = https://github.com/libretro/pocketsnes-libretro.git
LIBRETRO_ARMSNES_VERSION = master
LIBRETRO_ARMSNES_SITE = https://github.com/rmaz/ARMSNES-libretro.git
LIBRETRO_ARMSNES_SITE_METHOD = git
LIBRETRO_ARMSNES_TARGET = libpocketsnes.so

define LIBRETRO_ARMSNES_BUILD_CMDS
	$(MAKE) -f Makefile platform=armv6 CC="$(TARGET_CC)" CXX="$(TARGET_CXX)" O=$PWD \
		CFLAGS="$(TARGET_CFLAGS)" LD="$(TARGET_LD)" -C $(@D) all
endef

define LIBRETRO_ARMSNES_INSTALL_TARGET_CMDS
	$(INSTALL) -D $(@D)/$(LIBRETRO_ARMSNES_TARGET) \
		$(TARGET_DIR)/usr/lib/libretro/$(LIBRETRO_ARMSNES_TARGET)
endef

$(eval $(generic-package))
