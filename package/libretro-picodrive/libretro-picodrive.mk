################################################################################
#
# LIBRETRO_PICODRIVE
#
################################################################################
LIBRETRO_PICODRIVE_VERSION = master
LIBRETRO_PICODRIVE_SITE = https://github.com/libretro/picodrive.git
LIBRETRO_PICODRIVE_SITE_METHOD = git
LIBRETRO_PICODRIVE_TARGET = picodrive_libretro.so

define LIBRETRO_PICODRIVE_BUILD_CMDS	
	$(MAKE) -C $(@D)/cpu/cyclone cyclone_gen CONFIG_FILE=$(@D)/cpu/cyclone_config.h

	$(MAKE) -f Makefile.libretro platform="armv6 armasm" AS="$(TARGET_AS)" CC="$(TARGET_CC)" CXX="$(TARGET_CXX)" \
		LD="$(TARGET_LD)" -C $(@D) all
endef

define LIBRETRO_PICODRIVE_INSTALL_TARGET_CMDS
	$(INSTALL) -D $(@D)/$(LIBRETRO_PICODRIVE_TARGET) \
		$(TARGET_DIR)/usr/lib/libretro/$(LIBRETRO_PICODRIVE_TARGET)
endef

define LIBRETRO_PICODRIVE_EXTRACT_CMDS
	rm -rf $(@D)
	(git clone --depth 1 $(LIBRETRO_PICODRIVE_SITE) $(@D) && \
		cd $(@D) && \
		git submodule init && \
		git submodule update)
	touch $(@D)/.stamp_downloaded
endef


$(eval $(generic-package))
