################################################################################
#
# retroarch
#
################################################################################
RETROARCH_SHADERS_VERSION = master
RETROARCH_SHADERS_SITE = https://github.com/gizmo98/common-shaders
RETROARCH_SHADERS_SITE_METHOD = git

define RETROARCH_SHADERS_BUILD_CMDS
	
endef

define RETROARCH_SHADERS_INSTALL_TARGET_CMDS
	cp $(@D) $(TARGET_DIR)/usr/lib/libretro/shaders -R
	chmod 0755 $(TARGET_DIR)/usr/lib/libretro/shaders -R
endef

$(eval $(generic-package))
