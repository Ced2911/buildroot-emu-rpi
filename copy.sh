#! /bin/sh

export LC_ALL=C

if [ $# -ne 1 ]; then
	echo "Usage: $0 <drive>"
	exit 1;
fi

DRIVE=$1

mkdir /mnt/mountpointboot -p
mkdir /mnt/mountpointroot -p

sudo mount ${DRIVE}1 /mnt/mountpointboot
sudo mount ${DRIVE}2 /mnt/mountpointroot

cp output/images/rpi-firmware/* /mnt/mountpointboot
cp output/images/zImage /mnt/mountpointboot
sudo tar xf ./output/images/rootfs.tar -C /mnt/mountpointroot

sudo umount /mnt/mountpointboot
sudo umount /mnt/mountpointroot
